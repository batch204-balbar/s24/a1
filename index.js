let numInput = 14;
const getCube = Math.pow(numInput, 3);
resultCube = `The cube of ${numInput} is ${getCube}`
console.log(resultCube);

const fullAddress = ["234", " Purok 4", " Gulod" ];
const [houseNumber, street, city] = fullAddress;
resultAddress = `I live at ${fullAddress};`
console.log(resultAddress);

const animal = {
	name: "Dinosaur",
	type: "Pre-historic animal",
	weight: "500 kgs",
	height: "500 cm"
};
printAnimal = `${animal.name} was a ${animal.type}. It weighed at ${animal.weight} and a height of ${animal.height}`
console.log(printAnimal);

const numberArray = ["10", "9", "8", "7", "6"];

numberArray.forEach(function(numberArray) {
	console.log(numberArray);
});

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog("Bantay", "3 mos", "Aspin");
console.log(myDog);
